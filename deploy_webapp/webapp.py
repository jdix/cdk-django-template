import constructs
import aws_cdk as cdk
import configparser
import os
from aws_cdk import (
    Fn,
    RemovalPolicy,
    aws_ssm as _ssm,
    aws_secretsmanager as _secrets,
    aws_rds as _rds,
    aws_ec2 as _ec2,
    aws_ecs as _ecs,
    aws_ecs_patterns as _ecs_patterns,
    aws_ecr as _ecr,
    aws_ecr_assets as _ecr_assets,
    aws_autoscaling as _autoscaling,
    aws_route53 as _route53,
    aws_route53_targets as _route53_targets,
    aws_certificatemanager as _certificatemanager,
    aws_elasticloadbalancingv2 as _elbv2,
    aws_iam as _iam
)

DJANGO_PORT = os.environ["DJANGO_PORT"]

# class InfrastructureStack(cdk.Stack):

#     def __init__(self, scope: constructs.Construct, construct_id: str, **kwargs) -> None:
#         super().__init__(scope, construct_id, **kwargs)

class VariableStack(cdk.Stack):

    def __init__(self, scope: constructs.Construct, construct_id: str, deployment_environment=None, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        
        # consume context
        config_file = self.node.try_get_context("config_file")

        config_parser = configparser.ConfigParser()
        config_parser.read(config_file)

        for section in config_parser.sections():
            for var in config_parser[section]:

                # if not "secret" in var:
                _ssm.StringParameter(
                    self, var,
                    parameter_name=(deployment_environment + "_" + var + "_" + os.environ["PROJECT_SLUG"]).lower(),
                    string_value=config_parser[section][var],
                )
                # else:
                #     _secrets.Secret(
                #         self, var,
                #         generate_secret_string=_secrets.SecretStringGenerator(
                            
                #         )
                #     )

class DatabaseStack(cdk.Stack):

    def __init__(self, scope: constructs.Construct, construct_id: str, deployment_environment=None, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # can only have 1 subnet of a type in an az - https://github.com/aws/aws-cdk/issues/3626#issuecomment-521089930
        vpc = _ec2.Vpc(
            self, "vpc",
            max_azs=2,
            subnet_configuration=[
                _ec2.SubnetConfiguration(
                    name="isolated-subnet",
                    subnet_type=_ec2.SubnetType.PRIVATE_ISOLATED
                ),
                _ec2.SubnetConfiguration(
                    name="public-subnet",
                    subnet_type=_ec2.SubnetType.PUBLIC
                ),
                _ec2.SubnetConfiguration(
                    name="private-subnet",
                    subnet_type=_ec2.SubnetType.PRIVATE_WITH_NAT
                ),
            ],
            # nat_gateways=2
        )

        db_sg = _ec2.SecurityGroup(
            self, "sb-sg",
            vpc=vpc,
            allow_all_outbound=True,
        )

        snapshot = os.environ.get('DATABASE_SNAPSHOT_TO_RESTORE_FROM')
        if snapshot == None:
            rds_dbi = _rds.DatabaseInstance(
                self, "rds",
                database_name="mip_power_tools",
                engine=_rds.DatabaseInstanceEngine.postgres(
                    version=_rds.PostgresEngineVersion.VER_13_2
                ),
                vpc=vpc,
                port=5432,
                instance_type=_ec2.InstanceType.of(
                    _ec2.InstanceClass.BURSTABLE3,
                    _ec2.InstanceSize.SMALL,
                ),
                security_groups=[
                    db_sg
                ],
                vpc_subnets=_ec2.SubnetSelection(subnet_type=_ec2.SubnetType.PRIVATE_ISOLATED),
                removal_policy=RemovalPolicy.SNAPSHOT,
                # deletion_protection=True
            )
        else:
            rds_dbi = _rds.DatabaseInstanceFromSnapshot(
                self, "rds",
                snapshot_identifier=snapshot,
                # database_name="mip_power_tools",
                engine=_rds.DatabaseInstanceEngine.postgres(
                    version=_rds.PostgresEngineVersion.VER_13_2
                ),
                vpc=vpc,
                port=5432,
                instance_type=_ec2.InstanceType.of(
                    _ec2.InstanceClass.BURSTABLE3,
                    _ec2.InstanceSize.SMALL,
                ),
                security_groups=[
                    db_sg
                ],
                vpc_subnets=_ec2.SubnetSelection(subnet_type=_ec2.SubnetType.PRIVATE_ISOLATED),
                credentials=_rds.SnapshotCredentials.from_generated_secret(username="postgres")
                # removal_policy=RemovalPolicy.RETAIN,
                # deletion_protection=True
            )

        ssm_service_endpoint = _ec2.InterfaceVpcEndpoint(
            self, "ssmVpcEndpoint",
            vpc=vpc,
            service=_ec2.InterfaceVpcEndpointAwsService.SSM,
            )

        self.vpc = vpc
        self.rds_dbi = rds_dbi
        self.rds_dbi_sg = db_sg

class DjangoStack(cdk.Stack):

    def __init__(self, scope: constructs.Construct, construct_id: str, vpc: _ec2.Vpc, rds_dbi, rds_dbi_sg: _ec2.SecurityGroup, deployment_environment=None, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        # The code that defines your stack goes here

        # TODO Create a private link endpoint for ECS / ECR, a super small fraction of the
        # cost of ngw with all security of private subnets!
        # NOTE: Not a fraction of the cost. Could be technically more secure, however, email (ses) isn't supported over private link. 
        # ses over smtp IS (email-smtp), but we don't want to use that (more complicated)

        # ecs_service_endpoint = _ec2.InterfaceVpcEndpoint(
        #     self, "ecsVpcEndpoint",
        #     vpc=vpc,
        #     service=_ec2.InterfaceVpcEndpointAwsService.ECS,
        #     )

        # ecs_agent_service_endpoint = _ec2.InterfaceVpcEndpoint(
        #     self, "ecsAgentVpcEndpoint",
        #     vpc=vpc,
        #     service=_ec2.InterfaceVpcEndpointAwsService.ECS_AGENT,
        #     )

        # ecs_telemetry_service_endpoint = _ec2.InterfaceVpcEndpoint(
        #     self, "ecsTelemetryVpcEndpoint",
        #     vpc=vpc,
        #     service=_ec2.InterfaceVpcEndpointAwsService.ECS_TELEMETRY,
        #     )

        # secrets_manager_service_endpoint = _ec2.InterfaceVpcEndpoint(
        #     self, "secretsManagerVpcEndpoint",
        #     vpc=vpc,
        #     service=_ec2.InterfaceVpcEndpointAwsService.SECRETS_MANAGER,
        #     )

        # ecr_service_endpoint = _ec2.InterfaceVpcEndpoint(
        #     self, "ecrVpcEndpoint",
        #     vpc=vpc,
        #     service=_ec2.InterfaceVpcEndpointAwsService.ECR,
        #     )

        # ecr_docker_service_endpoint = _ec2.InterfaceVpcEndpoint(
        #     self, "ecrDockerVpcEndpoint",
        #     vpc=vpc,
        #     service=_ec2.InterfaceVpcEndpointAwsService.ECR_DOCKER,
        #     )

        # s3_gateway_endpoint = _ec2.GatewayVpcEndpoint(
        #     self, "s3VpcEndpoint",
        #     vpc=vpc,
        #     service=_ec2.GatewayVpcEndpointAwsService.S3,
        #     )

        # ses_gateway_endpoint = _ec2.InterfaceVpcEndpoint(
        #     self, "sesVpcEndpoint",
        #     vpc=vpc,
        #     service=_ec2.InterfaceVpcEndpointAwsService(
        #         name="email-smtp"
        #     ),
        # )

        ecs_cluster = _ecs.Cluster(
            self, "WebAppEcsCluster",
            vpc=vpc,
        )

        webapp_asg_sg = _ec2.SecurityGroup(
            self, "WebAppAsgSg",
            allow_all_outbound=True,
            vpc=vpc
        )

        self.webapp_asg_sg = webapp_asg_sg

# aws ec2 describe-images --filters "Name=name,Values=amzn2-ami-ecs-hvm-2.0.20210609-x86_64-ebs" --query "sort_by(Images, &CreationDate)[]" --profile uptime_pros --region us-east-2
#     [{
#         "Architecture": "x86_64",
#         ...
#         "ImageId": "ami-062be0c2f0e7fb6d2",
#         ...
#         "Description": "Amazon Linux AMI 2.0.20210609 x86_64 ECS HVM GP2",
#         ...
#         "Name": "amzn2-ami-ecs-hvm-2.0.20210609-x86_64-ebs",
#         ...
#     }]

        asg = _autoscaling.AutoScalingGroup(
            self, "MyAsg",
            instance_type=_ec2.InstanceType(os.environ["CONTAINER_INSTANCE_SIZE"]),
            machine_image=_ec2.MachineImage.lookup(
                name="amzn2-ami-ecs-hvm-2.0.20210609-x86_64-ebs",
                windows=False,
            ),
            vpc=vpc,
            vpc_subnets=_ec2.SubnetSelection(subnet_type=_ec2.SubnetType.PRIVATE_WITH_NAT),
            # new_instances_protected_from_scale_in=False, # seems to be overridden by capacity provider termination protection setting
            key_name="UptimePros",
            max_capacity=3,
            min_capacity=1,
            desired_capacity=1,
            security_group=webapp_asg_sg
        )

        asg_capacity_provider = _ecs.AsgCapacityProvider(
            self, "MyAsgCapacityProvider",
            auto_scaling_group=asg,
            enable_managed_termination_protection=False, # this prevents the destruction of the app completely at instance level, maybe good for production
        )

        ecs_cluster.add_asg_capacity_provider(
            asg_capacity_provider
        )
        
        # app tier
        app_task_def = _ecs.Ec2TaskDefinition(
            self, "AppTaskDef",
            network_mode=_ecs.NetworkMode.AWS_VPC,
        )

        app_image_asset = _ecr_assets.DockerImageAsset(
            self, "AppImageAsset",
            directory="src_webapp/app",
            file="app.Dockerfile",
        )

        app_image_asset.repository.grant_pull(app_task_def.task_role)
        rds_dbi.secret.grant_read(app_task_def.task_role)

        app_task_def.task_role.add_managed_policy(
            _iam.ManagedPolicy.from_aws_managed_policy_name(
                managed_policy_name="AmazonSESFullAccess"
            )
        )

        rds_secrets = _secrets.Secret.from_secret_complete_arn(self, "RdsSecrets", rds_dbi.secret.secret_full_arn)

        app_image = _ecs.ContainerImage.from_docker_image_asset(app_image_asset)

        app_task_def.add_container(
            "AppTaskDefContainer",
            image=app_image,
            # dns_search_domains="",
            memory_reservation_mib=256,
            memory_limit_mib=512,
            cpu=512,
            port_mappings=[
                _ecs.PortMapping(container_port=8000, host_port=8000, protocol=_ecs.Protocol.TCP)
            ],
            # TODO move some of these to secrets?
            environment={
                "DJANGO_SECRET_KEY":os.environ["DJANGO_SECRET_KEY"],
                "DJANGO_DEBUG":os.environ["DJANGO_DEBUG"],
                "DJANGO_PROJECT_NAME":os.environ["DJANGO_PROJECT_NAME"],
                "DJANGO_SUPERUSER_PASSWORD":os.environ["DJANGO_SUPERUSER_PASSWORD"],
                "AWS_DEFAULT_REGION":os.environ["AWS_DEFAULT_REGION"],
                "PASSWORD_RESET_EMAIL_DOMAIN_OVERRIDE":os.environ["PASSWORD_RESET_EMAIL_DOMAIN_OVERRIDE"],
            },
            # this vars existence tells the settings.py file to unpack the secrets from the json formatted secret payload into the normal db environment vars
            secrets={
                "RDS_SECRETS": _ecs.Secret.from_secrets_manager(rds_secrets)
            },
        )

        app_service = _ecs_patterns.ApplicationLoadBalancedEc2Service(
            self, "AppService",
            cluster=ecs_cluster,
            task_definition=app_task_def,
            listener_port=int(DJANGO_PORT),
            public_load_balancer=False,
            open_listener=False,
        )

        app_service.target_group.health_check = _elbv2.HealthCheck(
            enabled=True,
            healthy_http_codes="200",
            protocol=_elbv2.Protocol.HTTP,
            port=os.environ["DJANGO_PORT"],
            path='/accounts/login/',
            interval=cdk.Duration.seconds(30),
            healthy_threshold_count=5,
            unhealthy_threshold_count=2,
            timeout=cdk.Duration.seconds(5)
        )

        _ec2.CfnSecurityGroupIngress(
            self, "DbAllowingApp",
            ip_protocol="TCP",
            from_port=5432,
            to_port=5432,
            group_id=rds_dbi_sg.security_group_id, # Fn.import_value(deployment_environment + "RdsDbiSgIdTemplate"),
            source_security_group_id=app_service.service.connections.security_groups[0].security_group_id
        )

        # web tier
        web_task_def = _ecs.Ec2TaskDefinition(
            self, "WebTaskDef",
            # compatibility=_ecs.Compatibility.EC2,
            # cpu="1024",
            # memory_mib="1024",
            network_mode=_ecs.NetworkMode.AWS_VPC,
        )

        web_image_asset = _ecr_assets.DockerImageAsset(
            self, "WebImageAsset",
            directory="src_webapp/web",
            file="web.Dockerfile",
        )

        web_image_asset.repository.grant_pull(web_task_def.task_role)
        web_image = _ecs.ContainerImage.from_docker_image_asset(web_image_asset)

        web_task_def.add_container(
            "WebTaskDefContainer",
            image=web_image,
            # dns_search_domains="",
            memory_reservation_mib=256,
            memory_limit_mib=512,
            cpu=512,
            port_mappings=[
                _ecs.PortMapping(container_port=80, host_port=80, protocol=_ecs.Protocol.TCP)
            ],
            environment={
                "DJANGO_HOST": app_service.listener.load_balancer.load_balancer_dns_name,
                "DJANGO_PORT": DJANGO_PORT
            }
        )

        # web_service added after ssl


        # cdk.CfnOutput(
        #     self, (deployment_environment + "WebAppSgIdTemplate"),
        #     value=webapp_asg_sg.security_group_id,
        #     description="WebApp SG ID to allow Bastion Stack Later",
        #     export_name=(deployment_environment + "WebAppSgIdTemplate")
        # )

        base_domain_name = os.environ["DOMAIN_NAME"].split(".")[-2] + "." + os.environ["DOMAIN_NAME"].split(".")[-1]

        # dns zone ref
        dns_zone = _route53.HostedZone.from_lookup(self, "DnsZone",
            domain_name=base_domain_name
        )

        if deployment_environment == "prod":
            record_ttl_min=30
            dns_name = os.environ["DOMAIN_NAME"]
        else:
            record_ttl_min=1
            dns_name = deployment_environment + "." + os.environ["DOMAIN_NAME"]

        # ssl
        cert = _certificatemanager.Certificate(
            self, "webSslCert",
            domain_name=dns_name,
            # subject_alternative_names=[
            #     web_service.load_balancer.load_balancer_dns_name
            # ],
            validation=_certificatemanager.CertificateValidation.from_dns(
                hosted_zone=dns_zone
            )
        )

        web_service = _ecs_patterns.ApplicationLoadBalancedEc2Service(
            self, "WebService",
            cluster=ecs_cluster,
            task_definition=web_task_def,
            certificate=cert,
            redirect_http=True,
            # TODO - reuse lb by creating service, tg, etc manually https://github.com/aws/aws-cdk/issues/7327#issuecomment-614322050
            # load_balancer=app_service.load_balancer,
        )

        web_service.target_group.health_check = _elbv2.HealthCheck(
            enabled=True,
            healthy_http_codes="200",
            protocol=_elbv2.Protocol.HTTP,
            port="80",
            path='/accounts/login/',
            interval=cdk.Duration.seconds(30),
            healthy_threshold_count=5,
            unhealthy_threshold_count=2,
            timeout=cdk.Duration.seconds(5)
        )

        if base_domain_name == dns_name:
            _route53.ARecord(self, "aRecord",
                zone=dns_zone,
                target=_route53.RecordTarget.from_alias(_route53_targets.LoadBalancerTarget(web_service.load_balancer)),
                record_name=dns_name,
                ttl=cdk.Duration.minutes(record_ttl_min),
            )
        else:
            _route53.CnameRecord(self, "cnameRecord",
                zone=dns_zone,
                domain_name=web_service.load_balancer.load_balancer_dns_name,
                record_name=dns_name,
                ttl=cdk.Duration.minutes(record_ttl_min),
            )

        app_service.load_balancer.connections.security_groups[0].add_ingress_rule(
            peer=web_service.service.connections.security_groups[0],
            connection=_ec2.Port(
                protocol=_ec2.Protocol.TCP,
                from_port=8000,
                to_port=8000,
                string_representation="web tier to app tier"
            )
        )