FROM nginx:alpine

COPY nginx-template.conf .
COPY cmd-script.sh .
COPY static/ /usr/share/nginx/html/

RUN chmod 755 cmd-script.sh
# gettext = envsubst to replace env vars in config file
RUN apk add gettext

# COPY certificate.crt /etc/nginx/certs/certificate.crt
# COPY private.key /etc/nginx/certs/private.key

ENTRYPOINT ["/usr/bin/env"]
CMD ["sh", "/cmd-script.sh"]